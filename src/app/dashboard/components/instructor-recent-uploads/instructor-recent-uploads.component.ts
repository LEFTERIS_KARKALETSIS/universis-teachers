import { Component, OnInit } from '@angular/core';
import { CoursesService } from './../../../courses/services/courses.service';
import {ConfigurationService} from '@universis/common';

/**
 *
 * InstructorRecentUploadsComponent
 *
 * Fetches and displays information about the user's last upload actions
 *
 */

@Component({
  selector: 'app-instructor-recent-uploads',
  templateUrl: './instructor-recent-uploads.component.html'
})
export class InstructorRecentUploadsComponent implements OnInit {
  public actions = [];
  public isLoading = true;
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";

  constructor(private coursesService: CoursesService,
    private _configurationService: ConfigurationService) {
      this.currentLanguage = this._configurationService.currentLocale;
      this.defaultLanguage = this._configurationService.settings?.i18n?.defaultLocale;
     }

  ngOnInit() {
    this.loadData();
  }

  /**
   *
   * Fetches exams data and maps them to UploadAction objects.
   *
   */
  async loadData(): Promise<void> {
    try {
      const actions = await this.coursesService.getUploadHistoryRecent();
      this.actions = actions.filter((action) => {
        if (!action.object) {
          console.warn(`Upload action ${action.id} courseExam not found`);
        }
        return !!action.object;
      });
    } catch (err) {
      console.error(err);
    } finally {
      this.isLoading = false;
    }
  }

  getUrl(courseExamDocument: any): Array<any> {
    if (courseExamDocument
      && courseExamDocument.object
      && courseExamDocument.object.classes
      && courseExamDocument.object.classes.length > 0
    ) {
      const latestClass = courseExamDocument.object.classes.sort((a: any, b: any) => {
        return a.courseClass.period > b.courseClass.period ? -1 : 1;
      })[0];

      return ([
        '/courses',
        courseExamDocument.object.course.id,
        courseExamDocument.object.year.id,
        latestClass.courseClass.period,
        'exams',
        courseExamDocument.object.id
      ]);
    } else {
      return [];
    }
  }
}
