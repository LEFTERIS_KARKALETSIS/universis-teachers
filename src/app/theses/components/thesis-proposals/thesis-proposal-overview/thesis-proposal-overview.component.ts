import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, ToastService, ErrorService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-thesis-proposal-overview',
  templateUrl: './thesis-proposal-overview.component.html',
  styleUrls: ['./thesis-proposal-overview.component.scss']
})
export class ThesisProposalOverviewComponent implements OnInit, OnDestroy {

  dataSubscription: Subscription;
  paramSubscription: Subscription;
  @ViewChild('form') form?: AdvancedFormComponent;
  @ViewChild('notes', { static: true }) notes!: ElementRef;
  public formConfig: string;
  public model: any;
  private proposal: any;
  private notesEditor: any;
  public isLoading: boolean = true;
  public thesisRequestProposal: any;

  constructor(protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translate: TranslateService,
    private _loading: LoadingService,
    private _toastService: ToastService,
    private _errorService: ErrorService) {
  }

  ngOnInit(): void {
    this._loading.showLoading();
    this.isLoading = true;
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      if (params && params.hasOwnProperty('id'))
        this.proposal = params['id'];
        await this._context.model('Instructors/Me/thesisProposals')
                           .where('id')
                           .equal(this.proposal)
                           .expand('type,department,studyLevel,status,students')
                           .getItem().then(res => {
            this.model = res;
            this._loading.hideLoading();
            this.isLoading = false;
          }).catch(err => {
            console.log(err);
            return this._errorService.navigateToError(err);
          });
    });

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      if (data && data.hasOwnProperty('model')) {
        this.formConfig = 'Instructors/Me/Thesis/preview';
      }
    });
  }

  ngAfterViewInit() {
    //
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }

  }
}


