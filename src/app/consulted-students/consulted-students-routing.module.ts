import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@universis/common';
import { ConsultedStudentsHomeComponent } from './components/consulted-students-home/consulted-students-home.component';
import { ConsultedStudentsTableComponent } from './components/consulted-students-table/consulted-students-table.component';
// tslint:disable-next-line:max-line-length
import { ConsultedStudentsTableConfigurationResolver, ConsultedStudentsTableSearchResolver } from './components/consulted-students-table/consulted-students-table-config.resolver';
import { ConsultedStudentsDetailsComponent } from './components/consulted-students-details/consulted-students-details.component';
import { StudentDetailsGeneralComponent } from './components/consulted-students-details/student-details-general/student-details-general.component';
import { StudentDetailsNotesComponent } from './components/consulted-students-details/student-details-notes/student-details-notes.component';
import { StudentDetailsCoursesComponent } from './components/consulted-students-details/student-details-courses/student-details-courses.component';
import { StudentDetailsRegistrationsComponent } from './components/consulted-students-details/student-details-registrations/student-details-registrations.component';
import { AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormResolver } from '@universis/forms';
import { ConsultationsResolver, CounselorPrivateNoteResolver } from './consulted-students.resolver';
import { DeleteComponentOptions, GenericDeleteComponent } from '../generics/components/generic-delete/generic-delete.component';
import { ConsultedStudentsNotesComponent } from './components/consulted-students-notes/consulted-students-notes.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: "list",
        component: ConsultedStudentsHomeComponent,
        children: [
          {
            path: "notes",
            component: ConsultedStudentsNotesComponent,
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'CounselorPrivateNotes',
                  action: 'edit',
                  closeOnSubmit: true
                }
              },
              {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'CounselorPrivateNotes',
                  action: 'edit',
                  closeOnSubmit: true
                },
                resolve: {
                  data: AdvancedFormItemResolver,
                }
              },
              {
                path: ':id/delete',
                pathMatch: 'full',
                component: GenericDeleteComponent,
                outlet: 'modal',
                data: {
                  model: 'CounselorPrivateNotes',
                  action: 'delete',
                  message: 'ConsultedStudents.Notes.DeleteMessage'
                },
                resolve: {
                  data: AdvancedFormItemResolver,
                }
              }
            ]
          },
          {
            path: ':list',
            component: ConsultedStudentsTableComponent,
            resolve: {
              tableConfiguration: ConsultedStudentsTableConfigurationResolver,
              searchConfiguration: ConsultedStudentsTableSearchResolver
            }
          }
        ]
      },
      {
        path: ':studentId',
        component: ConsultedStudentsDetailsComponent,
        children: [
          {
            path: '',
            redirectTo: 'details',
            pathMatch: 'full'
          },
          {
            path: 'details',
            component: StudentDetailsGeneralComponent,
          },
          {
            path: 'notes',
            component: StudentDetailsNotesComponent,
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'Instructors/Me/CounselorPrivateNotes',
                  action: 'new',
                  closeOnSubmit: true
                },
                resolve: {
                  data: ConsultationsResolver,
                  formConfig: AdvancedFormResolver
                }
              },
              {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'Instructors/Me/CounselorPrivateNotes',
                  action: 'edit',
                  closeOnSubmit: true
                },
                resolve: {
                  data: CounselorPrivateNoteResolver,
                  formConfig: AdvancedFormResolver
                }
              },
              {
                path: ':id/delete',
                pathMatch: 'full',
                component: GenericDeleteComponent,
                data: <DeleteComponentOptions> {
                  model: 'Instructors/Me/CounselorPrivateNotes',
                  method: 'save',
                  confirmationModal: {
                    title: 'ConsultedStudents.Notes.DeleteAction.Modal.Title',
                    message: 'ConsultedStudents.Notes.DeleteAction.Modal.Message'
                  },
                  successToast: {
                    title: 'ConsultedStudents.Notes.DeleteAction.Toast.Title',
                    message: 'ConsultedStudents.Notes.DeleteAction.Toast.Message'
                  },
                  continueLink: '../..'
                },
                resolve: {
                  object: CounselorPrivateNoteResolver,
                }
              }
            ]
          },
          {
            path: 'courses',
            component: StudentDetailsCoursesComponent,
          },
          {
            path: 'registrations',
            component: StudentDetailsRegistrationsComponent,
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultedStudentsRoutingModule { }
