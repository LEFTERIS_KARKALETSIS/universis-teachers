import { Component } from '@angular/core';
import { BasicLoadComponent } from 'src/app/generics/components/BasicLoadComponent';
import { ResponseError } from '@themost/client';


@Component({
  selector: 'app-student-details-general',
  templateUrl: './student-details-general.component.html',
  styleUrls: []
})
export class StudentDetailsGeneralComponent extends BasicLoadComponent {

  public student: any;
  public studentFields: any;

  async load({ studentId }) {

    const result = await this.context.model('Instructors/me/consultedStudents')
      .where('student').equal(studentId)
      .expand('student($expand=studentStatus,department,studyProgram,inscriptionPeriod)')
      .getItem()

    if (typeof result === "undefined") {
      throw new ResponseError('Student not found', 404);
    }
    this.student = result.student;

    this.studentFields = [
      ["ConsultedStudents.GivenName", this.student.givenName],
      ["ConsultedStudents.FamilyName", this.student.familyName],
      ["ConsultedStudents.FatherName", this.student.fatherName],
      ["ConsultedStudents.StudentIdentifier", this.student.studentIdentifier],
      ["ConsultedStudents.InscriptionYear", this.student.inscriptionYear],
      ["ConsultedStudents.InscriptionPeriod", this.student.inscriptionPeriod.name],
      ["ConsultedStudents.Semester", this.student.semester],
      ["ConsultedStudents.StudentStatus", this.student.studentStatus.name],
      ["ConsultedStudents.Department", this.student.department.name],
      ["ConsultedStudents.StudyProgram", this.student.studyProgram.name],
    ]

  }

}
