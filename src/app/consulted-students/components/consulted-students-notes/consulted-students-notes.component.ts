import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService } from '@universis/common';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ResponseError } from '@themost/client';
import { BasicLoadComponent } from 'src/app/generics/components/BasicLoadComponent';


@Component({
  selector: 'app-consulted-students-notes',
  templateUrl: './consulted-students-notes.component.html',
  styleUrls: []
})
export class ConsultedStudentsNotesComponent extends BasicLoadComponent {

  public list = [];

  async load() {

    const list = await this.context.model('CounselorPrivateNotes')
      .asQueryable()
      .expand('student($select=StudentSummary)')
      .getItems()

    list.forEach(item => Object.assign(item, {
      studentRepr: `(${item.student.studentIdentifier}) ${item.student.familyName} ${item.student.givenName}`,
      // tableConfig: [
      //   ['ConsultedStudents.StudentIdentifier', item.student.studentIdentifier],
      //   ['ConsultedStudents.FullName', `${item.student.familyName} ${item.student.givenName}`],
      //   ['ConsultedStudents.Notes.dateCreated', item.dateCreated.toLocaleDateString()],
      //   ['ConsultedStudents.Notes.body', item.body],
      // ]
    }))

    this.list = list;
  }

}
