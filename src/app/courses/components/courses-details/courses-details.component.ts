import { Component, OnInit, Input } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ActivatedRoute, PRIMARY_OUTLET, Router, RoutesRecognized, UrlSegment, UrlSegmentGroup, UrlTree} from '@angular/router';
import {ErrorService, DiagnosticsService, ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-courses-details',
  templateUrl: './courses-details.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsComponent implements OnInit {
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";

  public courseClassList: any;
  public selectedCourseClass: any;

  private sub: any;
  public loading = true;
  public supportQA = false;
  public oneRosterEnabled: boolean = false;

  constructor(private coursesService: CoursesService,
              private errorService: ErrorService,
              private router: Router,
              private route: ActivatedRoute,
              private _diagnosticsService: DiagnosticsService,
              private _configurationService: ConfigurationService){
                this.currentLanguage = this._configurationService.currentLocale;
                this.defaultLanguage = this._configurationService.settings?.i18n?.defaultLocale;

  }

   ngOnInit() {
    this._diagnosticsService.hasService('QualityAssuranceService').then((result) => {
      this.supportQA = result;
    }).catch(err => {
      console.log(err);
    });

    this.route.params.subscribe(routeParams => {
      this.coursesService.getCourseClassList(routeParams['course']).then(courseClassList => {
        this.courseClassList = courseClassList;
        // get selected course class
        if (this.courseClassList) {
          this.selectedCourseClass = this.courseClassList.find( x =>
            x.year.id === parseInt(routeParams['year'], 10) && x.period.id === parseInt(routeParams['period'], 10));
        }
        this.loading = false;
      }).catch(err => {
        this.loading = false;
        return this.errorService.navigateToError(err);
      });
    });

    this._diagnosticsService.hasService('OneRosterService').then((result) => {
      if (result === true && !this._configurationService.settings.app['oneRosterDisabled']) {
        this.oneRosterEnabled = true;
      } else {
        this.oneRosterEnabled = false;
      }
    }).catch(err => {
      console.error(err);
    });
  }

  compareCourseClass(a, b) {
    return a && b && (a.year.id === b.year.id) && (a.period.id === b.period.id);
  }

  selectChangeHandler (event: any) {
    const tree: UrlTree = this.router.parseUrl(this.router.url);
    const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
    const s: UrlSegment[] = g.segments;
    const navigation = s.length >= 5 ? s[4].path : '';
    // navigate to course class
    return this.router.navigate([ '/courses', event.course.id, event.year.id, event.period.id, navigation ]);
  }

}
