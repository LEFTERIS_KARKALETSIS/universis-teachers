import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesDetailsComponent } from './courses-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from '@universis/common';
import { TestingConfigurationService } from 'src/app/test';
import { ErrorService } from '@universis/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@universis/common';

describe('CoursesDetailsComponent', () => {
  let component: CoursesDetailsComponent;
  let fixture: ComponentFixture<CoursesDetailsComponent>;

  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        MostModule.forRoot({
            base: '/',
            options: {
                useMediaTypeExtensions: false
            }
        })
    ],
      declarations: [ CoursesDetailsComponent ],
      providers: [
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show message when the user has no permissions to class', () => {
    component.selectedCourseClass = undefined; // the class is not found or the user has no permissions
    component.loading = false;
    fixture.detectChanges();
    const nativeElement = fixture.debugElement.nativeElement;
    const errorContainer = nativeElement.querySelector('.card');
    expect(errorContainer.textContent.trim()).toBe(
      'NotAvailableFeature.ClassNotAvailable'
    );
  });

  it('should not show the error when there is a course', () => {
    component.selectedCourseClass = {}; // truthy
    component.loading = false;
    fixture.detectChanges();
    const nativeElement = fixture.debugElement.nativeElement;
    const cards = nativeElement.querySelectorAll('.card');
    cards.forEach((card) => {
      expect(card.textContent).not.toBe('NotAvailableFeature.ClassNotAvailable');
    });
  });
});
