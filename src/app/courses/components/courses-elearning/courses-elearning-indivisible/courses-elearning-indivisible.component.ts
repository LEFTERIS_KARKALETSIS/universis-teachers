import { Component, Input, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';

@Component({
  selector: 'app-courses-elearning-indivisible',
  template: `
    <div class="lead font-weight-bold mb-2" [translate]="'CoursesLocal.CourseClassSections'"></div>
    <p class="card-text mb-2" innerHTML="{{ 'CoursesLocal.IndivisibleClass.IndivisibleCourseClassDescription' | translate }}"></p>
    <div>
      <ng-container *ngIf="courseClass && courseClass.sections.length > 0">
        <div class="mr-auto mb-3">
          <div *ngIf="isIndivisible" class="text-success mb-2" [translate]="'CoursesLocal.IndivisibleClass.IsAlreadyIndivisible'"></div>
          <div *ngIf="!isIndivisible" class="text-info mb-2" [translate]="'CoursesLocal.IndivisibleClass.IsNotIndivisible'"></div>
          <button *ngIf="enableActionsSetting" [disabled]="!(courseClass?.sections?.length > 0) || !this.enableActionsSetting || !this.supervisor || !this.currentClass"
            class="btn btn-outline-secondary mb-2" (click)="toggleIndivisible()">
            <ng-container [ngSwitch]="isIndivisible">
              <ng-container *ngSwitchCase="true">
                {{ 'CoursesLocal.IndivisibleClass.RemoveIndivisibilityStatus' | translate }}
              </ng-container>
              <ng-container *ngSwitchCase="false">
                {{ 'CoursesLocal.IndivisibleClass.MarkClassAsIndivisible' | translate }}
              </ng-container>
            </ng-container>
          </button>
          <div *ngIf="isIndivisible && enableActionsSetting">
            <input name="checkbox-let-section-active" id="checkbox-let-section-active" class="checkbox checkbox-secondary"
              type="checkbox" [checked]="letSectionActive === true" (change)="toggleSectionActive()"
              [disabled]="!(courseClass?.sections?.length > 0) || !this.enableActionsSetting || !this.supervisor || !this.currentClass"/>
            <label for="checkbox-let-section-active" [translate]="'CoursesLocal.IndivisibleClass.LetSectionActive'" [formControl]="sectionAliveCheckbox"></label>
          </div>
        </div>
      </ng-container>
    </div>
  `,
  styleUrls: []
})
export class CoursesElearningIndivisibleComponent implements OnInit {

  @Input('courseClass') courseClass: any;
  @Input('enableActionsSetting') enableActionsSetting: boolean = true;
  @Input('supervisor') supervisor: boolean = false;
  @Input('currentClass') currentClass: boolean = false;
  public isIndivisible: boolean = false;
  public letSectionActive: boolean = false;

  constructor(
    private context: AngularDataContext,
    private errorService: ErrorService,
    private loadingService: LoadingService,
  ) { }
  async ngOnInit(): Promise<void> {
    this.loadingService.showLoading();
    try {
      const req = await this.context.model(`instructors/me/classes/${this.courseClass.id}/oneroster/indivisible`).getItems();
      this.loadingService.hideLoading();
      if (req) {
        this.isIndivisible = req.indivisible;
        this.letSectionActive = req.letSectionActive;
      }
    } catch (err) {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.',
      });
      this.loadingService.hideLoading();
    }
  }

  async toggleIndivisible() {
    this.loadingService.showLoading();
    try {
      const req = await this.context.model(`instructors/me/classes/${this.courseClass.id}/oneroster/indivisible`).save({
        indivisible: !this.isIndivisible,
        letSectionActive: false
      });

      if (req) {
        this.isIndivisible = req.indivisible;
        this.letSectionActive = req.letSectionActive;
      }
      this.loadingService.hideLoading();
    } catch (err) {
      console.error(err)
      this.errorService.showError(err, {
        continueLink: '.',
      });
      this.loadingService.hideLoading();
    }
  }

  async toggleSectionActive() {
    this.loadingService.showLoading();
    try {
      const req = await this.context.model(`instructors/me/classes/${this.courseClass.id}/oneroster/indivisible`).save({
        letSectionActive: !this.letSectionActive
      });

      if (req) {
        this.letSectionActive = req.letSectionActive;
      }
      this.loadingService.hideLoading();
    } catch (err) {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.',
      });
      this.loadingService.hideLoading();
    }
  }
}
