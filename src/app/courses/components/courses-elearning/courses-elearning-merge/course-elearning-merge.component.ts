import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';


interface SelectableClass {
  id: string,
  courseDisplayCode: string,
  courseName: string,
  yearName: string,
  periodName: string,
  departmentName: string
}

interface CourseMerge {
  id: string,
  replacedBy: string,
  courseClass: string
}

@Component({
  selector: 'app-courses-elearning-merge',
  template: `
    <form>
      <div class="row" *ngIf="lastError">
        <div class="col-12">
          <div class="alert alert-warning" role="alert">
            <span>{{ 'CourseMergeModal.ErrorMessage.Title' | translate }}</span><br>
            <small *ngIf="lastError.status >= 0" [translate]="'E' + lastError.status + '.message'"></small>
            <small *ngIf="lastError.status == null">{{lastError.message}}</small>
          </div>
        </div>
      </div>
      <div class="mb-3" [translate]="'CourseMergeModal.Description'"></div>
      <select multiple class="form-control form-select" [size]="classes.length > 10 ? 10 : classes.length" [formControl]="selectionFormControl" [(ngModel)]="selection">
        <option *ngFor="let class of classes" (click)="okButtonDisabled = false">
          {{class.courseDisplayCode + " - " + class.courseName + " - " + class.yearName + "/" + class.periodName + " - " + class.departmentName}}
        </option>
      </select>
    </form>`
})
export class CoursesElearningMergeComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  private dataSubscription: Subscription;

  public classes: Array<SelectableClass>;
  public mergedClasses: Array<CourseMerge>;
  public mergedGroup: Array<string>;
  public currentClass: string;
  public currentClassTitle: string;
  public currentClassDisplayCode: string;
  private currentClassYearName: string;
  private currentClassPeriodName: string;
  private currentClassDepartmentName: string;
  public selectionFormControl = new FormControl();
  public isValid: boolean = false;
  public lastError: any;
  @Input() selection: Array<string>

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    private _context: AngularDataContext,
    private translate: TranslateService,
    private toastService: ToastService) {

    super(router, route);
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
    this.dataSubscription = this.activatedRoute.data.subscribe(async data => {
      this.okButtonDisabled = true;
      this.currentClass = data['courseClass']?.id;
      this.currentClassTitle = data['courseClass'].title;
      this.currentClassDisplayCode = data['courseClass'].course.displayCode;
      this.currentClassYearName = data['courseClass'].year.alternateName;
      this.currentClassPeriodName = data['courseClass'].period.name;
      this.currentClassDepartmentName = data['courseClass'].department.name;
      const currentClassYear = data['courseClass']?.year?.id;
      const currentClassPeriod = data['courseClass']?.period?.id;
      this.modalTitle = this.translate.instant('CourseMergeModal.Title', {
        displayCode: this.currentClassDisplayCode,
        class: this.currentClassTitle,
        yearName: this.currentClassYearName,
        periodName: this.currentClassPeriodName,
        departmentName: this.currentClassDepartmentName
      });

      // Get current merged classes relations in which this class participates
      this.mergedClasses = await this._context
        .model('instructors/me/classes/oneroster/replacements')
        .select('id, replacedBy, courseClass')
        .filter(`replacedBy/year eq ${currentClassYear} and courseClass/year eq ${currentClassYear} and replacedBy/period eq ${currentClassPeriod} and courseClass/period eq ${currentClassPeriod}`)
        .getItems();

      this.classes = await this._context
        .model('Instructors/me/currentClasses')
        .select(
          'id',
          'course/displayCode as courseDisplayCode',
          'course/name as courseName',
          'year/alternateName as yearName',
          'period/name as periodName',
          'department/name as departmentName'
        )
        .filter(`id ne '${this.currentClass}' and course/courseStructureType ne 4`)
        .orderBy('course/displayCode')
        .getItems();

      if (this.mergedClasses) {
        this.mergedGroup = this.mergedClasses.map(cl => cl.courseClass);
        // filter out courses that are already merged
        this.classes = this.classes.filter(element => !this.mergedGroup.includes(element.id));
      }
    });
  }

  async ok(): Promise<any> {
    try {
      this.lastError = null;
      for (const selectedClass of this.selection) {
        const findClass = this.classes.find(element => element.courseDisplayCode === selectedClass.split(",")[0]);
        const divisions = await this._context
          .model(`instructors/me/classes/${findClass.id}/oneroster/classes`)
          .select('id')
          .getItems();

        if (divisions.length > 0) {
          this.lastError = new Error(this.translate.instant('CourseMergeModal.ErrorMessage.Description', {displayCode: this.currentClassDisplayCode, class: findClass.courseName}));
          return;
        }
      }
      for (const selectedClass of this.selection) {
        await this._context.model('instructors/me/classes/oneroster/replacements')
          .save({
            courseClass: this.classes.find(element => element.courseDisplayCode === selectedClass.split(",")[0]),
            replacedBy: this.currentClass
          });
      }
      // show success toast message
      this.toastService.show(
        this.translate.instant('CourseMergeModal.SuccessTitle'),
        this.translate.instant(('CourseMergeModal.SuccessMessage'))
      );
      // close modal
      this.close({
        fragment: 'reload',
        skipLocationChange: true
      });
    } catch (err) {
      console.error(err);
      // show fail toast message
      this.toastService.show(
        this.translate.instant('CourseMergeModal.FailTitle'),
        this.translate.instant(('CourseMergeModal.FailMessage'))
      );
      // close modal without triggering a reload
      this.cancel();
    }
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
