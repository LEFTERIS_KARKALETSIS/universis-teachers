import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, LOCALE_ID, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { LocationStrategy, HashLocationStrategy, registerLocaleData } from '@angular/common';
import { AngularDataContext, ClientDataContextConfig, DATA_CONTEXT_CONFIG, MostModule } from '@themost/angular';
import { ConfigurationService, APP_LOCATIONS, UserService, LocalizedAttributesPipe, UserStorageService, SessionUserStorageService, LoadingService } from '@universis/common';
import { SharedModule } from '@universis/common';
import { ErrorModule } from '@universis/common';
import { AuthModule } from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgChartsModule } from 'ng2-charts';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AppSidebarModule } from '@coreui/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Routing Module
import { AppRoutingModule } from "./app.routing";
import { BreadcrumbsComponent } from './layouts/breadcrumb.component';
import { ProfileService } from './profile/services/profile.service';
import { ThesesModule } from './theses/theses.module';
import { StudentsModule } from './students/students.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TeachersSharedModule } from './teachers-shared/teachers-shared.module';
import { MessageSharedService } from './teachers-shared/services/messages.service';
import { QaModule } from '@universis/ngx-qa';
//import * as locations from './app.locations';
import { TEACHERS_APP_LOCATIONS } from './app.locations';
import { ConsultedStudentsSharedModule } from './consulted-students/consulted-students.shared';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './reducers';
import * as fromTables from '@universis/ngx-tables';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgxProfilesModule, NgxProfilesService, NgxProfilesUserService } from '@universis/ngx-profiles';
// LOCALES: import extra locales here
import el from '@angular/common/locales/el';
import en from '@angular/common/locales/en';
import {ReportsSharedModule} from '@universis/ngx-reports';
import {NgxSignerModule} from '@universis/ngx-signer';

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    BreadcrumbsComponent
  ],
  imports: [
    FormsModule,
    NgChartsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MostModule.forRoot({
      base: '/',
      options: {
        useMediaTypeExtensions: false,
        useResponseConversion: true
      }
    }),
    TranslateModule.forRoot(),
    RouterModule,
    SharedModule.forRoot(),
    TeachersSharedModule.forRoot(),
    AuthModule.forRoot(),
    AppRoutingModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ThesesModule,
    FormsModule,
    ReactiveFormsModule,
    StudentsModule,
    AppSidebarModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreModule.forFeature(fromTables.featureName, fromTables.tableReducers),
    ConsultedStudentsSharedModule.forRoot(),
    NgxProfilesModule.forRoot(),
    QaModule.forRoot(),
    NgxSignerModule.forRoot(),
    ReportsSharedModule.forRoot(),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      // use APP_INITIALIZER to load application configuration
      useFactory: (configurationService: ConfigurationService, context: AngularDataContext) =>
        async () => {
          // load application configuration
          await configurationService.load();
          context.setBase(configurationService.config.settings.remote.server);
          // load angular locales
          registerLocaleData(en);
          registerLocaleData(el);
          return true;
        },
      deps: [ConfigurationService, AngularDataContext],
      multi: true
    },
    {
      provide: UserStorageService,
      useValue: SessionUserStorageService,
    },
    {
      provide: APP_LOCATIONS,
      useValue: TEACHERS_APP_LOCATIONS,
    },
    {
      provide: LOCALE_ID,
      useFactory: (configurationService: ConfigurationService) => {
        return configurationService.currentLocale;
      },
      deps: [ConfigurationService],
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    ProfileService,
    MessageSharedService,
    LoadingService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {
  constructor() {
  }
}
