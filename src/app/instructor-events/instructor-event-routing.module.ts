import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { InstructorEventComponent } from './components/instructor-event/instructor-event.component';

const routes: Routes = [
  {
    path: '',
    component: InstructorEventComponent,
    data: {
      title: 'Teaching Events Schedule'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstructorEventRoutingModule { }
