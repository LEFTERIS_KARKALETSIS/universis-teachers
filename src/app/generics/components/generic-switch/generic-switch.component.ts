import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-generic-switch',
  templateUrl: './generic-switch.component.html',
  styleUrls: ['./generic-switch.component.scss']
})
export class GenericSwitchComponent implements OnInit {

  @Input() options: any[];

  @Input() selected!: any;
  @Output() selectedChange = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { }

  select(selected) {
    this.selected = selected;
    this.selectedChange.emit(this.selected);
  }

}
