import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppEventService, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

export declare interface DeleteComponentOptions {
  model: string;
  method: LocalMethodTypes;
  object: any;
  confirmationModal?: {
    title: string;
    message: string;
  };
  successToast?: {
    title: string;
    message: string;
  };
  continueLink?: string;
}

enum LocalMethodTypes {
  save = 'save',
  remove = 'remove'
}

@Component({
  selector: 'app-generic-delete',
  template: ``
})
export class GenericDeleteComponent implements OnInit, OnDestroy {
  constructor(
    private readonly modalService: ModalService,
    private readonly translateService: TranslateService,
    private readonly loadingService: LoadingService,
    private readonly context: AngularDataContext,
    private readonly errorService: ErrorService,
    private readonly toastService: ToastService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly appEvent: AppEventService
  ) {}

  private dataSubscription: Subscription;

  ngOnInit() {
    this.dataSubscription = this.activatedRoute.data.subscribe(async (data: DeleteComponentOptions) => {
      // extract options from route data
      const { model, method, confirmationModal, successToast, continueLink, object } = data;
      try {
        // validate essential component data
        if (!(model && method && object)) {
          throw new Error('Invalid component configuration');
        }
        // get confirmation about deletion
        const deletionConfirmation = await this.modalService.showWarningDialog(
          this.translateService.instant((confirmationModal && confirmationModal.title) || 'GenericDelete.Modal.Title'),
          this.translateService.instant((confirmationModal && confirmationModal.message) || 'GenericDelete.Modal.Message')
        );
        // if the action is cancelled
        if (deletionConfirmation !== 'ok') {
          // proceed to navigating to the provided continueLink, if any
          if (continueLink == null) {
            return;
          }
          return this.router.navigate([continueLink], {
            relativeTo: this.activatedRoute
          });
        }
        // show loading
        this.loadingService.showLoading();
        // if the item is to be removed by saving
        if (method === LocalMethodTypes.save) {
          // assign deletion state
          Object.defineProperty(object, '$state', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: 4
          });
        }
        // remove the item
        await this.context.model(model)[method](object);
        // show success toast
        this.toastService.show(
          this.translateService.instant((successToast && successToast.title) || 'GenericDelete.Toast.Title'),
          this.translateService.instant((successToast && successToast.message) || 'GenericDelete.Toast.Message')
        );
        // fire an app event
        this.appEvent.remove.next({
          model,
          target: object
        });
        // and finally navigate to the continueLink if any
        if (continueLink == null) {
          return;
        }
        return this.router.navigate([continueLink], {
          relativeTo: this.activatedRoute
        });
      } catch (err) {
        // log error
        console.error(err);
        // show error
        this.errorService.showError(err, {
          continueLink: continueLink || '.'
        });
      } finally {
        this.loadingService.hideLoading();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
