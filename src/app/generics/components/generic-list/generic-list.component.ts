import { Component, Input, OnInit, OnDestroy, TemplateRef, ContentChild, SimpleChanges, EventEmitter, Output } from '@angular/core';

export interface ItemConfig {
  header: [string, string][],
  body: [string, string, string?][],
  buttons: [string, any, string?][],
}

@Component({
  selector: 'app-generic-list',
  templateUrl: './generic-list.component.html',
  styleUrls: ['./generic-list.component.scss']
})
export class GenericListComponent implements OnInit, OnDestroy {

  // TODO start from view mode based on mobile or note
  // TODO store the last selected view mode
  // TODO autogenerate table config if no table is set (or rethink connection with tables)

  @Input() public list: any[];
  @ContentChild(TemplateRef) public itemTemplate: TemplateRef<any>;

  @Input() public filters: any[] | null;
  @Input() public sortingModes: any[] | null;
  @Input() public table: any | null;

  @Input() public buttons: any[] | null;

  public sortingMode = null;
  public searchText = "";
  public filteredList = [];

  public viewOption;
  public readonly viewOptions = [{
    name: "list",
    label: "GenericList.List",
    icon: "fas fa-list"
  }, {
    name: "table",
    label: "GenericList.Table",
    icon: "fas fa-table"
  }];

  @Output() viewOptionChange = new EventEmitter<any>();

  constructor() {
    this.viewOption = this.viewOptions[0];
  }

  ngOnInit() {
    this.sortingMode = this.sortingModes ? this.sortingModes[0] : null;
  }

  ngOnDestroy(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.search();
  }

  onViewOptionChange(value: any) {
    if (this.table) {
      // TODO handle the external table here
    }
    this.viewOptionChange.emit(value);
  }

  search() {
    if (!this.searchText) {
      this.filteredList = this.list;
    } else {
      this.filteredList = this.list
        .map(i => [this.matching(i, this.searchText), i])
        .filter(i => i[0])
        .sort(((a, b) => b[0] - a[0]))
        .map(i => i[1])
    }
    if (this.sortingMode) {
      const fields = this.sortingMode[1].split(',')
        .map(i => i.trim().split(' '));
      this.filteredList.sort((a, b) => {
        for (const [field, order] of fields) {
          const av = a[field], ab = b[field];
          if (av == ab) continue;
          return (av < ab ? -1 : 1) * (order == "asc" ? 1 : -1);
        }
        return 0;
      })
    }
  }

  matching(item: any, text: string, depth = 1) {
    const values = Object.values(item);
    for (const value of values) {
      if (!value) continue;
      if (typeof value == 'object') {
        const result = this.matching(value, text, depth + 1);
        if (result) return result;
      } else if (typeof value == 'number') {
        if (value.toString() == text) return -depth;
      } else if (typeof value == 'string') {
        if (value.toString().toLowerCase().includes(text.toLowerCase())) return -depth;
      }
    }
    return false;
  }

}
