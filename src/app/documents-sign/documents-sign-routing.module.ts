import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@universis/common';
import {DocumentsSignHomeComponent} from './components/documents-sign-home/documents-sign-home.component';
import {DocumentsSignTableComponent} from './components/documents-sign-table/documents-sign-table.component';
import {
  DocumentsSignTableConfigurationResolver,
  DocumentsSignTableSearchResolver
} from './components/documents-sign-table/documents-sign-table-config.resolver';
import { DocumentsSignMessageComponent } from './components/documents-sign-message/documents-sign-message.component';
import { DocumentsSignRejectComponent } from './components/documents-sign-reject/documents-sign-reject.component';
const routes: Routes = [
  {
    path: '',
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: "list",
        component: DocumentsSignHomeComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'active',
          },

          {
            path: ':list',
            component: DocumentsSignTableComponent,
            resolve: {
              tableConfiguration: DocumentsSignTableConfigurationResolver,
              searchConfiguration: DocumentsSignTableSearchResolver
            },
            children: [
              {
                path: ':action/cancelReason',
                component: DocumentsSignMessageComponent,
                outlet: 'modal'
              },
              {
                path: ':action/delete',
                component: DocumentsSignRejectComponent,
                outlet: 'modal'
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsSignRoutingModule { }
