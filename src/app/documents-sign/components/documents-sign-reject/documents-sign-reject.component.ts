import { AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import {AppEventService, ErrorService, LoadingService, ToastService, UserService} from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-documents-sign-reject',
  templateUrl: './documents-sign-reject.component.html',
  encapsulation: ViewEncapsulation.None,

})

export class DocumentsSignRejectComponent extends RouterModalOkCancel implements   AfterViewInit, OnDestroy {

  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  dataSubscription: Subscription;
  paramSubscription: Subscription;
  @ViewChild('form') form?: AdvancedFormComponent;
  @ViewChild('notes', { static: true }) notes: ElementRef;
  public formConfig: string ;
  public model: any;
  public documentID: any;
  public isLoading: boolean = true;


  constructor(protected _router: Router,
              protected _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translate: TranslateService,
              private _loading: LoadingService,
              private _toastService: ToastService,
              private _userService: UserService,
              private _appEvent: AppEventService,
              private _errorService: ErrorService,
  ) {
    super(_router, _activatedRoute);
    this.modalTitle = this._translate.instant('DocumentSignActions.RejectionTitle');
  }

  ngAfterViewInit(): void {
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      try {
        this.okButtonDisabled = true;
        this.isLoading = true;
        this._loading.showLoading();
        this.documentID = params['id'];
        this.formConfig = 'DocumentSignActions/reject';
        this.model = await this._context.model('DocumentSignActions')
          .where('id')
          .equal(params['action'])
          .expand('documentNumberSeriesItem')
          .getItem();
      } catch (err) {
        return this._errorService.navigateToError(err);
      } finally {
        this.isLoading = false;
        this._loading.hideLoading();
      }
    });
  }

  onChange(event: any) {
    this.okButtonDisabled = !this.form.form.formio.checkValidity();
  }

 ok(): Promise<any> {
    let documentCancel: any = {};
    if (this.form.form.formio.data) {
      documentCancel = this.form.form.formio.data;
      const cancelledDocument = {
        id: documentCancel.id,
        actionStatus: {alternateName: 'CancelledActionStatus'},
        cancelReason: documentCancel.cancelReason,
      }

      return this._context.model(`DocumentSignActions`).save(cancelledDocument)
        .then(async () => {
          // show success toast message
          this._toastService.show(
            this._translate.instant('DocumentSignActions.RejectionToast.SuccessTitle'),
            this._translate.instant('DocumentSignActions.RejectionToast.SuccessMessage')
          );
          // fire change event
          this._appEvent.change.next({
            model: 'DocumentSignActions',
            target: {
              id: documentCancel.id
            }
          });
          // close modal
          this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch(err => {
          // show fail toast message
          this._toastService.show(
            this._translate.instant('DocumentSignActions.RejectionToast.FailTitle'),
            this._translate.instant('DocumentSignActions.RejectionToast.FailMessage')
          );
          // close modal without triggering a reload
          this.cancel();
        });
    }
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}


