import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DocumentsSignTableComponent} from './documents-sign-table.component';


describe('DocumentsSignTableComponent', () => {
  let component: DocumentsSignTableComponent;
  let fixture: ComponentFixture<DocumentsSignTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsSignTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsSignTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
