import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {
  ActivatedTableService,
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent
} from '@universis/ngx-tables';
import {Observable,Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ErrorService, LoadingService, ModalService, ToastService, AppEventService } from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {DocumentSignActionComponent } from '@universis/ngx-signer'


@Component({
  selector: 'app-documents-sign-table',
  templateUrl: './documents-sign-table.component.html',
  styleUrls: ['./documents-sign-table.component.scss']
})
export class DocumentsSignTableComponent implements AfterViewInit, OnDestroy {

  private dataSubscription?: Subscription;
  public selectedItems: any[] = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table!: AdvancedTableComponent;
  @ViewChild('search') search!: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch!: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public recordsTotal: any;
  private fragmentSubscription?: Subscription;

  public viewOption;
  public list = [];
  public showList;
  public showSign: boolean = true;
  private changeSubscription: Subscription;
  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _context: AngularDataContext,
              private _toastService: ToastService,
              private _appEvent: AppEventService,
              private _http: HttpClient,
              private _translateService: TranslateService) {
  }


  ngAfterViewInit() {

    this.dataSubscription = this._activatedRoute.data.subscribe((data: { searchConfiguration: any, tableConfiguration: any }) => {
      try {
        this._activatedTable.activeTable = this.table;
        this.searchConfiguration = data.searchConfiguration;

        if (data['tableConfiguration']) {
          // set config
          this.table.config = AdvancedTableConfiguration.cast(data['tableConfiguration']);
          // reset search text
          this.advancedSearch.text = "";
          this.showList = false;
          // show or hide sign action
          this.showSign = this.table.config.columns .findIndex( (column) => {
            return column.property === 'signed';
          }) >= 0;
          // reset table
          this.table.reset(true);
          this._loadingService.showLoading();
        }
      } catch (err) {
        this._loadingService.hideLoading();
        this._errorService.navigateToError(err);
      }
    });

    this.changeSubscription = this._appEvent.changed.subscribe(async (event) => {
      if (!(this.table && this.table.dataTable)) {
        return;
      }
      if (!(event && event.target)) {
        return;
      }
      if (event.model === 'DocumentNumberSeriesItems') {
        try {
          // get table items in sync
          const signActions = this.table.dataTable.rows().data().toArray();
          if (!(Array.isArray(signActions) && signActions.length > 0)) {
            return;
          }
          // find the signAction that corresponds to the target series item
          const targetAction = signActions.find((action) => {
            return action.documentNumberSeriesItem === event.target.id;
          });
          if (targetAction == null) {
            return;
          }
          // and fetch/refresh it
          await this.table.fetchOne({
            id: targetAction.id
          });
        } catch (err) {
          // just log the error
          console.error(err);
        }
      }
      if (event.model === this.table.config.model) {
        try {
          await this.table.fetchOne({
            id: event.target.id
          });
        } catch (err) {
          console.error(err);
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  onDataLoad(event: AdvancedTableDataResult) {
    this.recordsTotal = event.recordsTotal;
  }

  async getSelectedItems() {
    let items: any[] = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id','actionStatus/alternateName as actionStatus'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus
            };
          });
        }
      }
    }
    return items;
  }

  async signAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(DocumentSignActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          item: {
          },
          items: this.selectedItems,
          modalTitle: 'DocumentSignActions.Edit.SignAction.Title',
          description: 'DocumentSignActions.Edit.SignAction.Description',
          extras: {
            documentCanBePublished: false
          },
          refresh: this.refreshAction,
          execute: this.executeSignAction()
        },
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  executeSignAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let i = 0; i < this.selectedItems.length; i++) {
          const item = this.selectedItems[i];
          const res: any = await this._executeSignActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  private _executeSignActionOne(item, index){
    return new Promise((resolve, reject) => {
      // set progress
      const total = this.selectedItems.length;
      this._context.model(this.table.config.model)
        .where('id').equal(item.id)
        .select('documentNumberSeriesItem', 'reportTemplateRole', 'owner')
        .expand('documentNumberSeriesItem', 'reportTemplateRole($expand=roleType($expand=titles))')
        .getItem().then((res) => {
        if (res == null) {
          return reject(new Error('The specified item cannot be found'));
        }
        // set component item which is the current document
        if (res.documentNumberSeriesItem == null) {
          return reject(new Error('The specified document cannot be found'));
        }
        if (res.reportTemplateRole == null) {
          return reject(new Error('The specified report template role cannot be found'));
        }
        const component = <DocumentSignActionComponent>this._modalService.modalRef.content;
        const document = res.documentNumberSeriesItem;
        // append the sign actions property to the document
        // so the document sign component can extract the role titles
        Object.defineProperty(document, 'signActions', {
          value: [res],
          enumerable: true,
          configurable: true,
          writable: true
        });
        setTimeout(() => {
          // sign document
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          component.sign(document).then(() => {
            // log success
            return this.table.fetchOne({
              id: item.id
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
          }).catch((err) => {
            // log error
            return resolve({
              success: 0,
              error: err
            });
          });
        }, 750);
      }).catch((err) => {
        // log error
        console.log(err);
        return resolve({
          success: 0,
          error: err
        });
      });
    });
  }

  async cancelAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus' ||  item.actionStatus === 'ActiveActionStatus'

      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(DocumentSignActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          item: {
          },
          items: this.selectedItems,
          modalTitle: 'DocumentSignActions.Edit.SignAction.Title',
          description: 'DocumentSignActions.Edit.SignAction.Description',
          refresh: this.refreshAction,
          execute: this.executeSignAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }







}
