import { TestBed } from '@angular/core/testing';

import { ProfileService } from './profile.service';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';

describe('ProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      SharedModule.forRoot(),
      MostModule.forRoot({
        base: '/',
        options: {
            useMediaTypeExtensions: false
        }
      })
    ],
    providers: [ProfileService]
  }));

  it('should be created', () => {
    const service: ProfileService = TestBed.get(ProfileService);
    expect(service).toBeTruthy();
  });
});
