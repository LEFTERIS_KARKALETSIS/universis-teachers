import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileHomeComponent } from './profile-home.component';
import { ProfilePreviewComponent } from '../profile-preview/profile-preview.component';
import { NgArrayPipesModule } from 'angular-pipes';
import { NgPipesModule } from 'ngx-pipes';
import { ProfileService } from '../../services/profile.service';
import { ErrorService } from '@universis/common';

describe('ProfileHomeComponent', () => {
  let component: ProfileHomeComponent;
  let fixture: ComponentFixture<ProfileHomeComponent>;

  const profileSvc = jasmine.createSpyObj('ProfleService,', ['getInstructor']);
  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);

  profileSvc.getInstructor.and.returnValue(Promise.resolve(JSON.parse('{}')));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgArrayPipesModule,
        NgPipesModule
      ],
      declarations: [ ProfileHomeComponent, ProfilePreviewComponent ],
      providers: [
        {
          provide: ProfileService,
          useValue: profileSvc
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
