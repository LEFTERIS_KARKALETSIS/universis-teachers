import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { DepartmentInfoComponent } from './components/department-info/department-info.component';
import { InfoPagesRoutingModule } from './info-pages-routing.module';
import { InfoPagesSharedModule } from './info-pages-shared.module';
import { InfoPagesHomeComponent } from './components/info-pages-home/info-pages-home.component';
import { SharedModule } from '@universis/common';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MostModule,
    InfoPagesRoutingModule,
    InfoPagesSharedModule,
    SharedModule
  ],
  declarations: [DepartmentInfoComponent, InfoPagesHomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InfoPagesModule { }
