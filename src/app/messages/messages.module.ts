import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MessagesRouting } from './messages.routing';
import { MessagesSharedModule } from './messages.shared';
import { SharedModule } from '@universis/common';
import { environment } from '../../environments/environment';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { SendMessageToClassComponent } from './components/send-message-to-class/send-message-to-class.component';
import {
  SendMessageToExamParticipantsComponent
} from './components/send-message-to-exam-participants/send-message-to-exam-participants.component';

// LOCALES: import extra locales here
import * as el from './i18n/messages.el.json';
import * as en from './i18n/messages.en.json';

@NgModule({
  imports: [
    CommonModule,
    MessagesRouting,
    TranslateModule,
    MessagesSharedModule,
    FormsModule,
    InfiniteScrollModule,
    SharedModule

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
  ],
  declarations: [MessagesHomeComponent, MessagesListComponent]
})
export class MessagesModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
